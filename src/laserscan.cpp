#include <laserscan.hpp>

int main( int argc, char** argv )
{
  ros::init( argc, argv, "laserscan_pub" );
  ros::Time::init();

  double ranges[ nReadings ];
  double intensities[ nReadings ];

  ros::NodeHandlePtr nh( new ros::NodeHandle );
  ros::Rate loop_rate( 10.0 );
  ros::Publisher laserScanPub = 
    nh -> advertise< sensor_msgs::LaserScan >( "LaserScan", 10 );

  tf::TransformBroadcaster broadcaster;
  tf::TransformListener listener;

  int count = 0;

  // Set min and max angles.
  double angle_min = -M_PI;
  double angle_max = M_PI;

  sensor_msgs::LaserScan laser_scan;

  while( ros::ok() )
  {
    ros::spinOnce();

    for( int i = 0; i < nReadings; i++ )
    {
      ranges[i] = (( count <= 5 ) ? count : 5 );
      intensities[i] = (( count <= 100 ) ? count : 100 );
    }

    laser_scan.header.frame_id = "box";
    laser_scan.header.stamp = ros::Time::now();
    laser_scan.angle_min = angle_min;
    laser_scan.angle_max = angle_max;
    laser_scan.angle_increment = ( angle_max - angle_min ) / nReadings;
    laser_scan.time_increment = ( 1 / freq * nReadings );
    laser_scan.range_min = 0.0;
    laser_scan.range_max = 1000000.0;
 
    laser_scan.ranges.resize(nReadings);
    laser_scan.intensities.resize(nReadings);

    for( int i = 0; i < nReadings; i++ )
    {
      laser_scan.ranges[i] = ranges[i];
      laser_scan.intensities[i] = intensities[i];
    }

    laserScanPub.publish( laser_scan );

    count++;
    loop_rate.sleep();
  }
}