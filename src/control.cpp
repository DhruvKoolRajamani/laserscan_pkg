#include "control.hpp"

int main(int argc, char** argv)
{
  ros::init(argc, argv, "control");

  ros::NodeHandlePtr n( new ros::NodeHandle );
  ros::Publisher odom_pub = n -> advertise< nav_msgs::Odometry >( "odom", 100 );
  tf::TransformBroadcaster odom_broadcaster;
  tf::TransformBroadcaster laser_broadcaster;

  double x = 0.0;
  double y = 0.0;
  double z = 0.47;
  double th = 0.0;

  double vx = 0.1;
  double vy = 0.0;
  double vth = 0.5;

  ros::Time current_time, last_time;
  current_time = ros::Time::now();
  last_time = ros::Time::now();

  ros::Rate r( 10.0 );
  while( n -> ok() )
  {
    ros::spinOnce();
    current_time = ros::Time::now();

    //compute odometry in a typical way given the velocities of the robot
    double dt = (current_time - last_time).toSec();
    double delta_x = (vx * cos(th) - vy * sin(th)) * dt;
    double delta_y = (vx * sin(th) + vy * cos(th)) * dt;
    double delta_th = vth * dt;

    x += delta_x;
    y += delta_y;
    th += delta_th;

    //since all odometry is 6DOF we'll need a quaternion created from yaw
    geometry_msgs::Quaternion odom_quat = tf::createQuaternionMsgFromYaw(th);

    //first, we'll publish the odom transform over tf
    geometry_msgs::TransformStamped odom_trans;
    odom_trans.header.stamp = current_time;
    odom_trans.header.frame_id = "map";
    odom_trans.child_frame_id = "base_link";

    odom_trans.transform.translation.x = x;
    odom_trans.transform.translation.y = y;
    odom_trans.transform.translation.z = z;
    odom_trans.transform.rotation = odom_quat;

    //send the transform
    odom_broadcaster.sendTransform(odom_trans);

    //first, we'll publish the laser transform over tf
    geometry_msgs::TransformStamped laser_trans;
    laser_trans.header.stamp = current_time;
    laser_trans.header.frame_id = "map";
    laser_trans.child_frame_id = "box";

    laser_trans.transform.translation.x = x;
    laser_trans.transform.translation.y = y;
    laser_trans.transform.translation.z = z + 0.4814;
    laser_trans.transform.rotation = odom_quat;

    //send the transform
    laser_broadcaster.sendTransform(laser_trans);

    //next, we'll publish the odometry message over ROS
    nav_msgs::Odometry odom;
    odom.header.stamp = current_time;
    odom.header.frame_id = "map";

    //set the position
    odom.pose.pose.position.x = x;
    odom.pose.pose.position.y = y;
    odom.pose.pose.position.z = z;
    odom.pose.pose.orientation = odom_quat;

    //set the velocity
    odom.child_frame_id = "base_link";
    odom.twist.twist.linear.x = vx;
    odom.twist.twist.linear.y = vy;
    odom.twist.twist.angular.z = vth;

    //publish the message
    odom_pub.publish(odom);

    last_time = current_time;
    r.sleep();
  }
}