# A Laser Scan package to test LaserScan messages

**This package utilizes the urdf of the R2D2 from ROS Beginner Tutorials**

In order to test this package, perform the following steps:

Open terminal

```bash
roscore
```

Open a new window **Ctrl+Alt+T** or a tab **Ctrl+Shift+T**

```bash
cd ~/catkin_ws/src/
git clone https://gitlab.com/DhruvKoolRajamani/laserscan_pkg.git
cd ~/catkin_ws/
catkin_make
source devel/setup.bash
roslaunch laserscan_pkg laserscan.launch
```